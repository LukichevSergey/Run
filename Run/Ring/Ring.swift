//
//  Ring.swift
//  Run
//
//  Created by Лукичев Сергей on 08.08.2023.
//

import SwiftUI

struct Ring: View {
    
    let lineWidth: CGFloat
    let backgroundColor: Color
    let foregroundColor: Color
    let percent: Double
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                RingShape()
                    .stroke(style: StrokeStyle(lineWidth: lineWidth))
                    .fill(backgroundColor)
                RingShape(percent: percent)
                    .stroke(style: StrokeStyle(lineWidth: lineWidth, lineCap: .round))
                    .fill(foregroundColor)
            }
            .animation(.easeIn(duration: 1), value: 1)
            .padding(lineWidth / 2)
        }
    }
}

struct Ring_Previews: PreviewProvider {
    static var previews: some View {
        Ring(lineWidth: 50, backgroundColor: .black.opacity(0.2), foregroundColor: .black, percent: 80)
    }
}
