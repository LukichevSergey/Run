//
//  RegistrationView.swift
//  Run
//
//  Created by Лукичев Сергей on 18.07.2023.
//

import SwiftUI

struct RegistrationView: View {
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var showAlert = false
    @State private var alertTitle = ""
    @State private var alertSubtitle = ""
    
    var body: some View {
        VStack {
            Text("Регистрация")
                .font(.largeTitle)
                .padding()
            
            TextField("Имя пользователя", text: $username)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            
            SecureField("Пароль", text: $password)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            
            Button(action: {
                registerUser()
            }) {
                Text("Зарегистрироваться")
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.blue)
                    .cornerRadius(10.0)
            }
            
            Spacer()
        }
        .padding()
        .alert(isPresented: $showAlert) {
            Alert(
                title: Text(alertTitle),
                message: Text(alertSubtitle),
                dismissButton: .default(Text("Ок")) {
                    self.alertTitle = ""
                    self.alertSubtitle = ""
                }
            )
        }
    }
    
    func registerUser() {
        AuthManager.shared.registerUser(withEmail: username, password: password) { userId, error in
            if let userId {
                UserManager.shared.createUser(id: userId, email: username) { user, error in
                    GlobalData.userModel = user
                }
            } else {
                alertTitle = "Ошибка регистрации"
                alertSubtitle = "Попробуйте еще раз"
            }
            showAlert = true
        }
    }
}

struct RegistrationView_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationView()
    }
}
