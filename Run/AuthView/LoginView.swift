//
//  LoginView.swift
//  Run
//
//  Created by Лукичев Сергей on 18.07.2023.
//

import SwiftUI

struct LoginView: View {
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var showErrorAlert = false
    
    var body: some View {
        VStack {
            Text("Авторизация")
                .font(.largeTitle)
                .padding()
            
            TextField("Имя пользователя", text: $username)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            
            SecureField("Пароль", text: $password)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            
            Button(action: {
                loginUser()
            }) {
                Text("Войти")
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.blue)
                    .cornerRadius(10.0)
            }
            
            Spacer()
        }
        .padding()
        .alert(isPresented: $showErrorAlert) {
            Alert(
                title: Text("Вход запрещен"),
                message: Text("Не верный логин / пароль"),
                dismissButton: .default(Text("Закрыть")) {
                    // Действие при закрытии алерта
                }
            )
        }
    }
    
    func loginUser() {
        AuthManager.shared.loginUser(withEmail: username, password: password) { userId, error in
            if let userId {
                UserManager.shared.getUserData(from: userId) { user, error in
                    GlobalData.userModel = user
                }
            } else {
                showErrorAlert = true
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
