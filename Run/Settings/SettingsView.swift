//
//  SettingsView.swift
//  Run
//
//  Created by Лукичев Сергей on 01.08.2023.
//

import SwiftUI

struct SettingsView: View {
    
    @StateObject private var model = SettingsViewModel()
    
    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            Text("Настройки")
                .font(.largeTitle)
            
            Spacer()
            
            VStack(alignment: .leading, spacing: 10) {
                Text("Имя")
                TextField("Введите Ваше имя", text: $model.name)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10.0).strokeBorder(Color.blue, style: StrokeStyle(lineWidth: 1.0)))
            }
            VStack(alignment: .leading, spacing: 10) {
                Text("Фамилия")
                TextField("Введите Вашу фамилию", text: $model.surname)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10.0).strokeBorder(Color.blue, style: StrokeStyle(lineWidth: 1.0)))
            }
            VStack(alignment: .leading, spacing: 10) {
                Text("Вес")
                TextField("Введите Ваш вес", value: $model.weight, formatter: NumberFormatter())
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 10.0).strokeBorder(Color.blue, style: StrokeStyle(lineWidth: 1.0)))
                    .keyboardType(.decimalPad)
            }
            DatePicker("Дата рождения", selection: $model.dateOfBirthday, displayedComponents: [.date])

            Button(action: {
                model.saveUserData()
            }) {
                Text("Сохранить")
                    .font(.title)
                    .padding()
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(10)
                    .frame(maxWidth: .infinity)
            }
            .buttonStyle(.borderedProminent)
            Button(action: {
                GlobalData.userModel = nil
            }) {
                Text("Выйти")
                    .font(.title)
                    .padding()
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(10)
                    .frame(maxWidth: .infinity)
            }
            .buttonStyle(.borderedProminent)
            
            Spacer()
        }
        .padding(.horizontal)
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
