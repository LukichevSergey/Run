//
//  SettingsViewModel.swift
//  Run
//
//  Created by Лукичев Сергей on 08.08.2023.
//

import SwiftUI

class SettingsViewModel: ObservableObject {

    var name: String
    var surname: String
    var dateOfBirthday: Date
    var weight: Double
    
    init(name: String? = nil, surname: String? = nil, dateOfBirthday: String? = nil, weight: Double? = nil) {
        self.name = GlobalData.userModel?.name ?? ""
        self.surname = GlobalData.userModel?.surname ?? ""
        self.dateOfBirthday = GlobalData.userModel?.dateOfBirthday ?? Date()
        self.weight = GlobalData.userModel?.weight ?? 70
    }
    
    func saveUserData() {
        UserManager.shared.updateUserData(name: name, surname: surname, weight: weight, dateOfBirthDay: dateOfBirthday)
    }
}
