//
//  MapTrackView.swift
//  Run
//
//  Created by Лукичев Сергей on 19.07.2023.
//

import SwiftUI
import CoreLocation
import MapKit

struct MapTrackView: View {
    
    @StateObject private var trainingManager = TrainingManager.shared
    @StateObject private var locationManager = LocationManager()
    
    @State private var trackingStarted = false
    @State private var isLocationAuthorized = false
    @State private var routeCoordinates: [CLLocationCoordinate2D] = []
    @State private var totalDistance: CLLocationDistance = 0
    @State private var temp: String = "0:00"
    @State private var stepsCount: Int = 0
    @State private var region: MKCoordinateRegion? = nil

    var body: some View {
        VStack {
            ZStack {
                MapView(routeCoordinates: routeCoordinates,
                        userLocation: locationManager.location) { region in
                    self.region = region
                }
                    .edgesIgnoringSafeArea(.all)
            }
            VStack {
                HStack {
                    Text("Время тренировки: \(trainingManager.elapsedTime.formatTime())")
                    Spacer()
                }
                HStack {
                    Text("Темп: \(temp) мин/км")
                    Spacer()
                }
                HStack {
                    Text("Пройденное расстояние: \(String(format: "%.2f", totalDistance / 1000)) км")
                    Spacer()
                }
                HStack {
                    Text("Кол-во шагов: \(stepsCount)")
                    Spacer()
                }
                .padding(.bottom)

                HStack {
                    
                    Spacer()
                    
                    Button(action: {
                        trackingStarted.toggle()
                        if trackingStarted {
                            locationManager.startTracking()
                            trainingManager.startTimer()
                        } else {
                            locationManager.stopTracking()
                            trainingManager.stopTimer()
                        }
                    }) {
                        Text(trackingStarted ? "Пауза" : "Старт")
                            .font(.title)
                            .padding()
                            .background(Color.blue)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                    }
                    
                    Spacer()
                    
                    Button(action: {
                        let mapRegion = MapRegion(centerLatitude: region?.center.latitude,
                                                  centerLongitude: region?.center.longitude,
                                                  spanLatitudeDelta: region?.span.latitudeDelta,
                                                  spanLongitudeDelta: region?.span.longitudeDelta)
                        let training = Training(trainingTime: trainingManager.elapsedTime.formatTime(),
                                                pastUpset: totalDistance,
                                                mapRegion: mapRegion,
                                                stepsCount: stepsCount)

                        trackingStarted = false
                        routeCoordinates.removeAll()
                        totalDistance = 0
                        stepsCount = 0
                        temp = "0:00"
                        
                        locationManager.stopTracking()
                        trainingManager.stopTimer()
                        trainingManager.resetTimer()
                        trainingManager.saveTraining(training: training)
                    }) {
                        Text("Стоп")
                            .font(.title)
                            .padding()
                            .background(Color.blue)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                    }
                    
                    Spacer()
                }

            }
            .padding()
        }
        .onAppear {
            if let userId = GlobalData.userModel?.id {
                trainingManager.setUser(with: userId)
            }
            locationManager.requestAuthorization()
            trainingManager.fetchTrainings()
        }
        .onReceive(locationManager.$location) { location in
            guard let location = location else { return }

            if trackingStarted {
                routeCoordinates.append(location.coordinate)
                totalDistance = locationManager.calculateDistance(routeCoordinates: routeCoordinates)
                stepsCount = trainingManager.stepCount
                temp = trainingManager.calculatePace(distance: totalDistance,
                                                     time: trainingManager.elapsedTime)
            }
        }
        .onReceive(locationManager.$isLocationAuthorized) { authorized in
            isLocationAuthorized = authorized
        }
    }
}

struct MapTrackView_Previews: PreviewProvider {
    static var previews: some View {
        MapTrackView()
    }
}
