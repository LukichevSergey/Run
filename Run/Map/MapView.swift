//
//  MapView.swift
//  Run
//
//  Created by Лукичев Сергей on 19.07.2023.
//

import SwiftUI
import MapKit
import CoreLocation

struct MapView: UIViewRepresentable {
    let coefficient: Double = 33.3
    var routeCoordinates: [CLLocationCoordinate2D]
    var userLocation: CLLocation?
    var currentZoomScale: MKZoomScale = 15
    var zoom: Double {
        get { return currentZoomScale * coefficient }
    }
    
    // Define the closure type
    var onReturnValue: ((MKCoordinateRegion) -> Void)?
    
    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView()
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .follow
        mapView.delegate = context.coordinator
        return mapView
    }
    
    func updateUIView(_ uiView: MKMapView, context: Context) {
        if !routeCoordinates.isEmpty {
            var coordinates = routeCoordinates
            let polyline = MKPolyline(coordinates: &coordinates, count: coordinates.count)
            uiView.addOverlay(polyline)
            let region = MKCoordinateRegion(center: routeCoordinates.last!, latitudinalMeters: zoom, longitudinalMeters: zoom)
            uiView.setRegion(region, animated: true)
            onReturnValue?(region)
        }
        
        if let userLocation = userLocation {
            uiView.removeAnnotations(uiView.annotations)
            let annotation = MKPointAnnotation()
            annotation.coordinate = userLocation.coordinate
            uiView.addAnnotation(annotation)

            let region = MKCoordinateRegion(center: userLocation.coordinate, latitudinalMeters: zoom, longitudinalMeters: zoom)
            uiView.setRegion(region, animated: true)
        }
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, MKMapViewDelegate {
        
        private var parent: MapView
        
        init(_ parent: MapView) {
            self.parent = parent
        }
        
        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
            if let polyline = overlay as? MKPolyline {
                let renderer = MKPolylineRenderer(polyline: polyline)
                renderer.strokeColor = .red
                renderer.lineWidth = 5
                return renderer
            }
            return MKOverlayRenderer()
        }
        
        func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
            // Получаем текущий зум
            parent.currentZoomScale = mapView.visibleMapRect.size.width / Double(mapView.bounds.size.width)
            
            // Печатаем значение зума в консоль
            print("Текущий зум: \(parent.currentZoomScale)")
        }
    }
}
