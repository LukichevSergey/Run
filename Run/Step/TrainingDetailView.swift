//
//  TrainingDetailView.swift
//  Run
//
//  Created by Лукичев Сергей on 07.08.2023.
//

import SwiftUI
import MapKit

struct TrainingDetailView: View {
    
    let training: Training
    @State var mapImage: Image? = nil
    var distancePercent: Double {
        return ((training.pastUpset > 0 ? training.pastUpset : 1) / 1000) * 100
    }
    var stepsPercent: Double {
        return ((Double(training.stepsCount > 0 ? training.stepsCount : 1)) / 1000) * 100
    }
    
    var body: some View {
        VStack {
            if let mapImage = mapImage {
                mapImage
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            } else {
                Text("Loading map...")
            }
            ZStack {
                VStack {
                    Text("\(String(format: "%.0f", training.pastUpset)) из 1 000 метров")
                    Text("\(String(format: "%.0f", Double(training.stepsCount))) из 1 000 шагов")
                }
                Ring(lineWidth: 25,
                     backgroundColor: .green.opacity(0.2),
                     foregroundColor: .green,
                     percent: distancePercent)
                .frame(width: 300, height: 300)
                Ring(lineWidth: 20,
                     backgroundColor: .blue.opacity(0.2),
                     foregroundColor: .blue,
                     percent: stepsPercent)
                .frame(width: 250, height: 250)
            }
        }
        .onAppear {
            let options = MKMapSnapshotter.Options()
            options.region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: training.mapRegion?.centerLatitude ?? 0, longitude: training.mapRegion?.centerLongitude ?? 0), span: MKCoordinateSpan(latitudeDelta: training.mapRegion?.spanLatitudeDelta ?? 0, longitudeDelta: training.mapRegion?.spanLongitudeDelta ?? 0))
            
            let snapshotter = MKMapSnapshotter(options: options)
            snapshotter.start(with: DispatchQueue.global(qos: .background)) { snapshot, error in
                guard let snapshot = snapshot, error == nil else {
                    return
                }
                
                let image = snapshot.image
                let mapImage = Image(uiImage: image)
                DispatchQueue.main.async {
                    self.mapImage = mapImage
                }
            }
        }
    }
}

struct TrainingDetailView_Previews: PreviewProvider {
    static var previews: some View {
        TrainingDetailView(training: .init(trainingTime: "00:03:19", pastUpset: 425, mapRegion: .init(centerLatitude: 55.755786, centerLongitude: 37.617633, spanLatitudeDelta: 0.004486365538295961, spanLongitudeDelta: 0.00795564412625944), stepsCount: 963))
    }
}
