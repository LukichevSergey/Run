//
//  TrainingTableViewCell.swift
//  Run
//
//  Created by Лукичев Сергей on 31.07.2023.
//

import SwiftUI

struct TrainingTableViewCell: View {
    
    let training: Training
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(training.type.trainingTitle)
                    .font(.title)
                Text("\(String(format: "%.2f", training.pastUpset / 1000)) км.")
                    .font(.title2)
            }
            Spacer()
            VStack(alignment: .trailing) {
                Text("\(training.date.formatted())")
                Text(training.trainingTime)
            }
        }
        .padding()
        .overlay(
            RoundedRectangle(cornerRadius: 16)
                .stroke(.green, lineWidth: 4)
        )
    }
}

struct TrainingTableViewCell_Previews: PreviewProvider {
    static var previews: some View {
        TrainingTableViewCell(training: .init(trainingTime: "00:03:19", pastUpset: 536, mapRegion: .init(centerLatitude: 12.3, centerLongitude: 157.3, spanLatitudeDelta: 111.7, spanLongitudeDelta: 122.5), stepsCount: 344))
    }
}
