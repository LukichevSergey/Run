//
//  StepView.swift
//  Run
//
//  Created by Лукичев Сергей on 19.07.2023.
//

import SwiftUI
import CoreMotion

struct StepView: View {
    @StateObject private var trainingManager = TrainingManager.shared
    @State private var stepCount: Int = 0
    @State private var distance: Double = 0.0
    @State private var workouts: [Training] = []
    
    let pedometer = CMPedometer()
    
    var body: some View {
        NavigationView {
            VStack {
                Spacer()
                Text("Шаги: \(stepCount)")
                    .font(.title)
                    .foregroundColor(.red)
                Text("Дистанция: \(String(format: "%.2f", distance)) м")
                    .font(.title)
                    .foregroundColor(.red)
                Text("Всего тернировок: \(workouts.count)")
                    .font(.title)
                    .foregroundColor(.red)
                Spacer()
                List(workouts, id: \.self) { workout in
                    NavigationLink(destination: TrainingDetailView(training: workout)) {
                        TrainingTableViewCell(training: workout)
                            .listRowSeparator(.hidden)
                    }
                }
                .listStyle(.plain)
            }
            .onAppear {
                if let userId = GlobalData.userModel?.id {
                    trainingManager.setUser(with: userId)
                }
                startTrackingStepCount()
                startTrackingDistance()
                trainingManager.fetchTrainings()
            }
            .onReceive(trainingManager.$trainings) { trainings in
                workouts = trainings
            }
        }
    }
    
    func startTrackingStepCount() {
        if CMPedometer.isStepCountingAvailable() {
            let calendar = Calendar.current
            let today = calendar.startOfDay(for: Date())
            
            pedometer.startUpdates(from: today) { data, error in
                if let steps = data?.numberOfSteps.intValue {
                    stepCount = steps
                }
            }
        }
    }
    
    func startTrackingDistance() {
        if CMPedometer.isDistanceAvailable() {
            let calendar = Calendar.current
            let today = calendar.startOfDay(for: Date())
            
            pedometer.startUpdates(from: today) { data, error in
                if let distance = data?.distance?.doubleValue {
                    self.distance = distance
                }
            }
        }
    }
}

struct StepView_Previews: PreviewProvider {
    static var previews: some View {
        StepView()
    }
}
