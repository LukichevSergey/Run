//
//  GlobalData.swift
//  Run
//
//  Created by Лукичев Сергей on 31.07.2023.
//

import Foundation
import Combine

let GlobalData = GlobalDataContainer.shared

class GlobalDataContainer {
    
    static let shared = GlobalDataContainer()
    
    private let userDefault = UserDefaults.standard
    
    private init() {
        let userDefault = UserDefaults.standard
        
        userModel = {
            if let data = userDefault.object(forKey: "userModel") as? Data,
               let profile = try? JSONDecoder().decode(User.self, from: data) { return profile }

            return nil
        }()
    }
    
    @Published var userModel: User? {
        didSet {
            if let encoded = try? JSONEncoder().encode(userModel) {
                userDefault.set(encoded, forKey: "userModel")
            } else {
                userDefault.set(nil, forKey: "userModel")
            }
        }
    }
}
