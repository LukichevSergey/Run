//
//  MapRegion.swift
//  Run
//
//  Created by Лукичев Сергей on 08.08.2023.
//

import Foundation
import MapKit

struct MapRegion: Codable {
    var centerLatitude: CLLocationDegrees?
    var centerLongitude: CLLocationDegrees?
    var spanLatitudeDelta: CLLocationDegrees?
    var spanLongitudeDelta: CLLocationDegrees?
    
    init(centerLatitude: CLLocationDegrees?, centerLongitude: CLLocationDegrees?, spanLatitudeDelta: CLLocationDegrees?, spanLongitudeDelta: CLLocationDegrees?) {
        self.centerLatitude = centerLatitude
        self.centerLongitude = centerLongitude
        self.spanLatitudeDelta = spanLatitudeDelta
        self.spanLongitudeDelta = spanLongitudeDelta
    }
    
    init?(from dictionary: [String: Any]) {
        if let centerLatitude = dictionary["centerLatitude"] as? CLLocationDegrees,
           let centerLongitude = dictionary["centerLongitude"] as? CLLocationDegrees,
           let spanLatitudeDelta = dictionary["spanLatitudeDelta"] as? CLLocationDegrees,
           let spanLongitudeDelta = dictionary["spanLongitudeDelta"] as? CLLocationDegrees {
            self.centerLatitude = centerLatitude
            self.centerLongitude = centerLongitude
            self.spanLatitudeDelta = spanLatitudeDelta
            self.spanLongitudeDelta = spanLongitudeDelta
        } else {
            return nil
        }
    }
    
    func toDictionary() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        
        if let centerLatitude = centerLatitude {
            dictionary["centerLatitude"] = centerLatitude
        }
        if let centerLongitude = centerLongitude {
            dictionary["centerLongitude"] = centerLongitude
        }
        if let spanLatitudeDelta = spanLatitudeDelta {
            dictionary["spanLatitudeDelta"] = spanLatitudeDelta
        }
        if let spanLongitudeDelta = spanLongitudeDelta {
            dictionary["spanLongitudeDelta"] = spanLongitudeDelta
        }
        
        return dictionary
    }
}
