//
//  Training.swift
//  Run
//
//  Created by Лукичев Сергей on 01.08.2023.
//

import Foundation

class Training: Codable, Hashable {
    
    static func == (lhs: Training, rhs: Training) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    @frozen enum TrainingType: String, Codable {
        case run
        case walk
        
        var trainingTitle: String {
            switch self {
            case .run: return "Бег"
            case .walk: return "Ходьба"
            }
        }
        
        static func fromString(_ string: String) -> TrainingType? {
            if string == "run" {
                return .run
            } else if string == "walk" {
                return .walk
            }
            return .run
        }
    }
    
    let id: UUID
    let date: Date
    let type: TrainingType
    let trainingTime: String
    let pastUpset: Double // Дистанция в метрах
    let stepsCount: Int
    let mapRegion: MapRegion?
    
    init(trainingTime: String, pastUpset: Double, mapRegion: MapRegion, stepsCount: Int) {
        self.id = UUID()
        self.date = Date()
        self.type = .run
        self.trainingTime = trainingTime
        self.pastUpset = pastUpset
        self.stepsCount = stepsCount
        self.mapRegion = mapRegion
    }
    
    init?(from dictionary: [String: Any]) {
        guard let uuidString = dictionary["id"] as? String,
              let uuid = UUID(uuidString: uuidString),
              let dateString = dictionary["date"] as? String,
              let date = ISO8601DateFormatter().date(from: dateString),
              let typeString = dictionary["type"] as? String,
              let type = TrainingType.fromString(typeString),
              let trainingTime = dictionary["trainingTime"] as? String,
              let pastUpset = dictionary["pastUpset"] as? Double,
              let stepsCount = dictionary["stepsCount"] as? Int else {
            return nil
        }
        
        self.id = uuid
        self.date = date
        self.type = type
        self.trainingTime = trainingTime
        self.pastUpset = pastUpset
        self.stepsCount = stepsCount
        if let mapRegionDict = dictionary["mapRegion"] as? [String: Any] {
            self.mapRegion = MapRegion(from: mapRegionDict)
        } else {
            self.mapRegion = nil
        }
    }
    
    func toDictionary() -> [String: Any] {
        var dict: [String: Any] = [:]
        dict["id"] = id.uuidString
        dict["date"] = ISO8601DateFormatter().string(from: date)
        dict["type"] = type.rawValue
        dict["trainingTime"] = trainingTime
        dict["pastUpset"] = pastUpset
        dict["stepsCount"] = stepsCount
        dict["mapRegion"] = mapRegion?.toDictionary()
        return dict
    }
}
