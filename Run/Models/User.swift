//
//  User.swift
//  Run
//
//  Created by Лукичев Сергей on 31.07.2023.
//

import Foundation

class User: Codable {
    let id: String
    let name: String?
    let surname: String?
    let dateOfBirthday: Date?
    let weight: Double?
    var trainings: [Training]
    
    init(with id: String) {
        self.id = id
        self.name = nil
        self.surname = nil
        self.dateOfBirthday = nil
        self.weight = nil
        self.trainings = []
    }

    init?(from dictionary: [String: Any]) {
        guard let id = dictionary["id"] as? String else { return nil }

        self.id = id
        self.name = dictionary["name"] as? String
        self.surname = dictionary["surname"] as? String
        self.weight = dictionary["weight"] as? Double
        self.trainings = (dictionary["trainings"] as? [[String: Any]])?.compactMap { Training(from: $0) } ?? []
        if let dateString = dictionary["dateOfBirthday"] as? String {
            self.dateOfBirthday = ISO8601DateFormatter().date(from: dateString)
        } else {
            self.dateOfBirthday = nil
        }
    }
}
