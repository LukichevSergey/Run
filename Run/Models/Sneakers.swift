//
//  Sneakers.swift
//  Run
//
//  Created by Лукичев Сергей on 01.08.2023.
//

import Foundation

struct Sneakers {
    let id: String = UUID().uuidString
    let color: String
    let cost: Double
    let size: Int
    
    func toDictionary() -> [String: Any] {
        return [
            "color": color,
            "cost": cost,
            "size": size
        ]
    }
}
