//
//  TrainingManager.swift
//  Run
//
//  Created by Лукичев Сергей on 31.07.2023.
//

import Foundation
import Firebase
import CoreMotion

final class TrainingManager: ObservableObject {
    
    let pedometer = CMPedometer()
    
    @Published var trainings: [Training] = []
    @Published var isRunning = false
    @Published var elapsedTime = 0.0
    @Published var stepCount = 0
    
    private var timer: Timer?
    
    let database = Database.database().reference()
    var userId: String?
    
    static let shared = TrainingManager()
    
    private init() {}
    
    func startTimer() {
        isRunning = true
        startTrackingStepCount()
        timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
            self.elapsedTime += 0.1
        }
    }

    func stopTimer() {
        isRunning = false
        timer?.invalidate()
        timer = nil
    }

    func resetTimer() {
        isRunning = false
        elapsedTime = 0.0
        timer?.invalidate()
        timer = nil
        stopTrackingStepCount()
    }
    
    func setUser(with userId: String) {
        self.userId = userId
    }
    
    func startTrackingStepCount() {
        if CMPedometer.isStepCountingAvailable() {
            
            pedometer.startUpdates(from: Date()) { data, error in
                if let steps = data?.numberOfSteps.intValue {
                    self.stepCount = steps
                }
            }
        }
    }
    
    func stopTrackingStepCount() {
        pedometer.stopUpdates()
        stepCount = 0
    }
    
    func fetchTrainings() {
        guard trainings.isEmpty else { return }
        guard let userId else { return }
        
        let userRef = database.child("users").child(userId)
        
        userRef.child("trainings").getData { error, snapShot in
            guard let snapShot else { return }
            if let userDict = snapShot.value as? [[String : Any]] {
                for training in userDict {
                    if let newTraining: Training = .init(from: training) {
                        self.trainings.append(newTraining)
                    }
                }
            }
        }
    }
    
    func saveTraining(training: Training) {
        trainings.append(training)
        guard let userId else { return }
        
        let userRef = database.child("users").child(userId)
        
        userRef.getData { error, snapShot in
            guard let snapShot else { return }
            if var userDict = snapShot.value as? [String : Any] {
                
                // Получите текущий массив тренировок пользователя
                var trainings = userDict["trainings"] as? [[String: Any]] ?? []
                // Добавьте новую тренировку в массив
                trainings.append(training.toDictionary())
                // Обновите данные пользователя с новым полем тренировок
                userDict["trainings"] = trainings
                
                userRef.setValue(userDict) { (error, ref) in
                    if let error = error {
                        print("Failed to update user: \(error.localizedDescription)")
                    } else {
                        print("User updated successfully!")
                    }
                }
            }
        }
    }
    
    func calculatePace(distance: Double, time: Double) -> String {
        guard distance > 0 else { return "0:00" }
        let pace = time / distance // Расчет темпа (в секундах на метр)
        let minutes = Int(pace / 60) // Получение целых минут
        let seconds = Int(pace.truncatingRemainder(dividingBy: 60)) // Получение оставшихся секунд
        
        return String(format: "%d:%02d", minutes, seconds) // Возврат времени в формате "минуты:секунды"
    }
}


