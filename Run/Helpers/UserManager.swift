//
//  FirebaseManager.swift
//  Run
//
//  Created by Лукичев Сергей on 18.07.2023.
//

import Foundation
import Firebase

class UserManager {
    
    static let shared = UserManager()
    
    private init() {}
    
    let database = Database.database().reference()
    
    func createUser(id: String, email: String, completion: @escaping (User, Error?) -> Void) {
        let userRef = database.child("users").child(id)
        let userData: [String: Any] = [
            "id": id,
            "email": email
        ]
        
        userRef.setValue(userData) { (error, ref) in
            if let error = error {
                print("Failed to create user: \(error.localizedDescription)")
            } else {
                print("User created successfully!")
                completion(User(with: id), error)
            }
        }
    }
    
    func getUserData(from id: String, completion: @escaping (User?, Error?) -> Void) {
        let userRef = database.child("users").child(id)
        
        userRef.getData { error, snapShot in
            guard let snapShot else { return }
            if let userDict = snapShot.value as? [String : Any] {
                let user = User(from: userDict)
                completion(user, error)
            }
        }
    }
    
    func updateUserData(name: String?, surname: String?, weight: Double?, dateOfBirthDay: Date?) {
        guard let userId = GlobalData.userModel?.id else { return }
        let userRef = database.child("users").child(userId)
        
        userRef.getData { error, snapShot in
            guard let snapShot else { return }
            if var userDict = snapShot.value as? [String : Any] {
                
                userDict["name"] = name
                userDict["surname"] = surname
                userDict["weight"] = weight
                if let dateOfBirthDay {
                    userDict["dateOfBirthday"] = ISO8601DateFormatter().string(from: dateOfBirthDay)
                }
                
                userRef.setValue(userDict) { (error, ref) in
                    if let error = error {
                        print("Failed to update user: \(error.localizedDescription)")
                    } else {
                        print("User updated successfully!")
                    }
                }
            }
        }
    }
    
    func calculateAge(birthDate: Date) -> Int {
        let calendar = Calendar.current
        let currentDate = Date()
        let components = calendar.dateComponents([.year], from: birthDate, to: currentDate)
        let age = components.year ?? 0
        return age
    }
}
