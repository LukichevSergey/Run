//
//  AuthManager.swift
//  Run
//
//  Created by Лукичев Сергей on 17.07.2023.
//

import Foundation
import FirebaseAuth

class AuthManager {
    static let shared = AuthManager()

    private init() {}

    func registerUser(withEmail email: String, password: String, completion: @escaping (String?, Error?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
            if let error = error {
                completion(nil, error)
            } else if let id = authResult?.user.uid {
                completion(id, nil)
            } else {
                completion(nil, nil)
            }
        }
    }

    func loginUser(withEmail email: String, password: String, completion: @escaping (String?, Error?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
            if let error = error {
                completion(nil, error)
            } else if let id = authResult?.user.uid {
                completion(id, nil)
            } else {
                completion(nil, nil)
            }
        }
    }
}

