//
//  ContentView.swift
//  Run
//
//  Created by Лукичев Сергей on 04.07.2023.
//

import SwiftUI
import FirebaseAuth
import CoreLocation
import CoreMotion

struct ContentView: View {
    
    @State var isAuth: Bool = false
    
    var body: some View {
        if isAuth {
            TabView() {
                MapTrackView()
                    .tabItem {
                        Image(systemName: "map")
                        Text("Карта")
                    }
                StepView()
                    .tabItem {
                        Image(systemName: "figure.step.training")
                        Text("Шаги")
                    }
                SettingsView()
                    .tabItem {
                        Image(systemName: "gearshape")
                        Text("Настройки")
                    }
            }.onReceive(GlobalData.$userModel) { user in
                isAuth = user != nil
            }
        } else {
            TabView() {
                LoginView()
                .tabItem {
                    Image(systemName: "lock.circle")
                    Text("Авторизация")
                }
                RegistrationView()
                    .tabItem {
                        Image(systemName: "person.crop.circle")
                        Text("Регистрация")
                    }
            }.onReceive(GlobalData.$userModel) { user in
                isAuth = user != nil
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
